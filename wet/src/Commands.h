#ifndef SMASH_COMMAND_H_
#define SMASH_COMMAND_H_

#include <vector>

const static unsigned int COMMAND_ARGS_MAX_LENGTH = 200;
const static unsigned int COMMAND_MAX_ARGS = 20;

class Command {
        // TODO: Add your data members
    public:
        Command(const char *command_line);
        virtual ~Command();
        virtual void execute() = 0;
        // virtual void prepare();
        // virtual void cleanup();
        //  TODO: Add your extra methods if needed
};

class BuiltInCommand : public Command {
    public:
        BuiltInCommand(const char *command_line);

        virtual ~BuiltInCommand() {
        }
};

class ExternalCommand : public Command {
    public:
        ExternalCommand(const char *command_line);

        virtual ~ExternalCommand() {
        }

        void execute() override;
};

class PipeCommand : public Command {
        // TODO: Add your data members
    public:
        PipeCommand(const char *command_line);

        virtual ~PipeCommand() {
        }

        void execute() override;
};

class RedirectionCommand : public Command {
        // TODO: Add your data members
    public:
        explicit RedirectionCommand(const char *command_line);

        virtual ~RedirectionCommand() {
        }

        void execute() override;
        // void prepare() override;
        // void cleanup() override;
};

class ChangeDirCommand : public BuiltInCommand {
        // TODO: Add your data members public:
        ChangeDirCommand(const char *command_line, char **p_last_pwd);

        virtual ~ChangeDirCommand() {
        }

        void execute() override;
};

class GetCurrDirCommand : public BuiltInCommand {
    public:
        GetCurrDirCommand(const char *command_line);

        virtual ~GetCurrDirCommand() {
        }

        void execute() override;
};

class ShowPidCommand : public BuiltInCommand {
    public:
        ShowPidCommand(const char *command_line);

        virtual ~ShowPidCommand() {
        }

        void execute() override;
};

class JobsList;

class QuitCommand : public BuiltInCommand {
        // TODO: Add your data members
    public:
        QuitCommand(const char *command_line, JobsList *jobs);

        virtual ~QuitCommand() {
        }

        void execute() override;
};

class JobsList {
    public:
        class JobEntry {
                // TODO: Add your data members
        };

        // TODO: Add your data members
    public:
        JobsList();
        ~JobsList();
        void add_job(Command *command, bool is_stopped = false);
        void print_jobs_list();
        void kill_all_jobs();
        void remove_finished_jobs();
        JobEntry *get_job_by_id(int job_id);
        void remove_job_by_id(int job_id);
        JobEntry *get_last_job(int *last_job_id);
        JobEntry *get_last_stopped_job(int *job_id);
        // TODO: Add extra methods or modify existing ones as needed
};

class JobsCommand : public BuiltInCommand {
        // TODO: Add your data members
    public:
        JobsCommand(const char *command_line, JobsList *jobs);

        virtual ~JobsCommand() {
        }

        void execute() override;
};

class ForegroundCommand : public BuiltInCommand {
        // TODO: Add your data members
    public:
        ForegroundCommand(const char *command_line, JobsList *jobs);

        virtual ~ForegroundCommand() {
        }

        void execute() override;
};

class BackgroundCommand : public BuiltInCommand {
        // TODO: Add your data members
    public:
        BackgroundCommand(const char *command_line, JobsList *jobs);

        virtual ~BackgroundCommand() {
        }

        void execute() override;
};

class TimeoutCommand : public BuiltInCommand {
        /* Bonus */
        // TODO: Add your data members
    public:
        explicit TimeoutCommand(const char *command_line);

        virtual ~TimeoutCommand() {
        }

        void execute() override;
};

class ChmodCommand : public BuiltInCommand {
        // TODO: Add your data members
    public:
        ChmodCommand(const char *command_line);

        virtual ~ChmodCommand() {
        }

        void execute() override;
};

class GetFileTypeCommand : public BuiltInCommand {
        // TODO: Add your data members
    public:
        GetFileTypeCommand(const char *command_line);

        virtual ~GetFileTypeCommand() {
        }

        void execute() override;
};

class SetcoreCommand : public BuiltInCommand {
        // TODO: Add your data members
    public:
        SetcoreCommand(const char *command_line);

        virtual ~SetcoreCommand() {
        }

        void execute() override;
};

class KillCommand : public BuiltInCommand {
        // TODO: Add your data members
    public:
        KillCommand(const char *command_line, JobsList *jobs);

        virtual ~KillCommand() {
        }

        void execute() override;
};

class SmallShell {
    private:
        // TODO: Add your data members
        SmallShell();

    public:
        Command *CreateCommand(const char *command_line);
        SmallShell(const SmallShell &) = delete;  // disable copy ctor
        void operator=(const SmallShell &) = delete;  // disable = operator

        static SmallShell &get_instance()  // make SmallShell singleton
        {
            static SmallShell instance;  // Guaranteed to be destroyed.
            // Instantiated on first use.
            return instance;
        }

        ~SmallShell();
        void execute_command(const char *command_line);
        // TODO: add extra methods as needed
};

#endif  // SMASH_COMMAND_H_
