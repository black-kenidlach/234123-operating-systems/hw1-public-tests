#include "Commands.h"

#include <iomanip>
#include <iostream>
#include <sstream>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include <vector>


const std::string WHITESPACE = " \n\r\t\f\v";

#if 0
#define FUNC_ENTRY() cout << __PRETTY_FUNCTION__ << " --> " << endl;

#define FUNC_EXIT() cout << __PRETTY_FUNCTION__ << " <-- " << endl;
#else
#define FUNC_ENTRY()
#define FUNC_EXIT()
#endif

static std::string ltrim(const std::string &line) {
    const std::size_t start = line.find_first_not_of(WHITESPACE);
    return (start == std::string::npos) ? "" : line.substr(start);
}

static std::string rtrim(const std::string &line) {
    const std::size_t end = line.find_last_not_of(WHITESPACE);
    return (end == std::string::npos) ? "" : line.substr(0, end + 1);
}

static std::string trim(const std::string &line) {
    return rtrim(ltrim(line));
}

static int parse_command_line(const char *command_line, char **args) {
    FUNC_ENTRY()
    int i = 0;
    std::istringstream iss(trim(std::string{command_line}).c_str());
    for (std::string s; iss >> s;) {
        args[i] = (char *) malloc(s.length() + 1);
        memset(args[i], 0, s.length() + 1);
        strcpy(args[i], s.c_str());
        args[++i] = NULL;
    }
    return i;

    FUNC_EXIT()
}

static bool is_background_command(const char *command_line) {
    const std::string str{command_line};
    return str[str.find_last_not_of(WHITESPACE)] == '&';
}

static void remove_background_sign(char *command_line) {
    const std::string str{command_line};
    // find last character other than spaces
    const int idx = str.find_last_not_of(WHITESPACE);
    // if all characters are spaces then return
    if (idx == std::string::npos) {
        return;
    }
    // if the command line does not end with & then return
    if (command_line[idx] != '&') {
        return;
    }
    // replace the & (background sign) with space and then remove all tailing spaces.
    command_line[idx] = ' ';
    // truncate the command line string up to the last non-space character
    command_line[str.find_last_not_of(WHITESPACE, idx) + 1] = 0;
}

// TODO: Add your implementation for classes in Commands.h

SmallShell::SmallShell() {
    // TODO: add your implementation
}

SmallShell::~SmallShell() {
    // TODO: add your implementation
}

/**
 * Creates and returns a pointer to Command class which matches the given command line (command_line)
 */
Command *SmallShell::CreateCommand(const char *command_line) {
    // For example:
    /*
      string cmd_s = _trim(string(command_line));
      string firstWord = cmd_s.substr(0, cmd_s.find_first_of(" \n"));

      if (firstWord.compare("pwd") == 0) {
        return new GetCurrDirCommand(command_line);
      }
      else if (firstWord.compare("showpid") == 0) {
        return new ShowPidCommand(command_line);
      }
      else if ...
      .....
      else {
        return new ExternalCommand(command_line);
      }
      */
    return nullptr;
}

void SmallShell::execute_command(const char *command_line) {
    // TODO: Add your implementation here
    // for example:
    // Command* cmd = CreateCommand(command_line);
    // cmd->execute();
    // Please note that you must fork smash process for some commands (e.g., external commands....)
}
