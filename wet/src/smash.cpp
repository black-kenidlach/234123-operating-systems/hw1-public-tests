#include "Commands.h"
#include "signals.h"

#include <iostream>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    if (signal(SIGTSTP, ctrl_z_handler) == SIG_ERR) {
        perror("smash error: failed to set ctrl-Z handler");
    }
    if (signal(SIGINT, ctrl_c_handler) == SIG_ERR) {
        perror("smash error: failed to set ctrl-C handler");
    }

    // TODO: setup sig alarm handler

    SmallShell &smash = SmallShell::get_instance();
    while (true) {
        std::cout << "smash> ";
        std::string command_line;
        std::getline(std::cin, command_line);
        smash.execute_command(command_line.c_str());
    }
    return 0;
}
