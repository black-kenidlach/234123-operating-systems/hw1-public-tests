#ifndef SMASH__SIGNALS_H_
#define SMASH__SIGNALS_H_

void ctrl_z_handler(int signal_number);
void ctrl_c_handler(int signal_number);
void alarm_handler(int signal_number);

#endif  // SMASH__SIGNALS_H_
