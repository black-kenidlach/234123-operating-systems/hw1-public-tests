#!/usr/bin/env sh

set -Eeuxo pipefail

cmake -S . -B build "$@"
cmake --build build
