#!/usr/bin/env sh

set -Eeuxo pipefail

if [[ $# -gt 0 ]]; then
    clang-format -i -Werror $@
else
    clang-format -i -Werror src/*.cpp src/*.h test/*.cc
fi
