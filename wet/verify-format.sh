#!/usr/bin/env sh

set -Eeuxo pipefail

if [[ $# -gt 0 ]]; then
    clang-format --dry-run -Werror $@
else
    clang-format --dry-run -Werror src/*.cpp src/*.h test/*.cc
fi
