#!/usr/bin/env sh

set -Eeuxo pipefail

cmake -S . -B build "$@"
cmake --build build
oldpwd=$(pwd)
cd build
    ctest --output-on-failure
cd "${oldpwd}"
