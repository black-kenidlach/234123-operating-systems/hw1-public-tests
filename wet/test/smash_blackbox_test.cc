#include "Commands.h"
#include "signals.h"

#include <gtest/gtest.h>
#include <regex>
#include <sys/sysinfo.h>

TEST(Official, input1) {
    const std::vector<std::string> input{"sleep 1",
                                         "chprompt hello",
                                         "sleep 2",
                                         "sleep 3",
                                         "sleep 5&",
                                         "chprompt",
                                         "sleep 10&",
                                         "sleep 12",
                                         "quit kill"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : input) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};

    EXPECT_STREQ(captured_out.c_str(), "smash: sending SIGKILL signal to 0 jobs:\n");
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;
}

TEST(Smash, chprompt) {
    const std::vector<std::string> input{"chprompt foo_bar", "chprompt"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : input) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_TRUE(captured_out.empty()) << "out.str() = " << captured_out;
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;
}

TEST(Smash, chprompt_ext) {
    const std::vector<std::string> in{"chprompt 1&",
                                      "chprompt a another_param and_another_one",
                                      "chprompt very_long_name_with_numbers123_&symbols",
                                      "chprompt&"};

    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_TRUE(captured_out.empty()) << "out.str() = " << captured_out;
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;
}

TEST(Smash, showpid) {
    const std::vector<std::string> in{"showpid", " showpid extra_param &"};

    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};

    const std::string pid{std::to_string(getpid())};
    EXPECT_STREQ(captured_out.c_str(), ("smash pid is " + pid + "\nsmash pid is " + pid + "\n").c_str());
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;
}

TEST(Smash, pwd) {
    const std::vector<std::string> in{"pwd"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    char *const cwd{getcwd(nullptr, 0)};
    ASSERT_NE(cwd, nullptr);
    std::string cwd_str{cwd};
    free(cwd);
    EXPECT_EQ(captured_out, cwd_str + "\n");
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;
}

TEST(Smash, cd_no_oldpwd) {
    const std::vector<std::string> in{"cd -\n"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_TRUE(captured_out.empty()) << "out.str() = " << captured_out;
    EXPECT_STREQ(captured_err.c_str(), "smash error: cd: OLDPWD not set\n");
}

TEST(Smash, cd_oldpwd_ok) {
    const std::vector<std::string> in{"cd /", "pwd", "cd /home", "pwd", "cd -", "pwd", "cd -", "pwd"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_STREQ(captured_out.c_str(), "/\n/home\n/\n/home\n");
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;
}

TEST(Smash, pwd_cd_errors) {
    const std::vector<std::string> in{
        "cd /",
        "cd not_real extra_param",
        "pwd extra_param",
    };
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_STREQ(captured_out.c_str(), "/\n");
    EXPECT_STREQ(captured_err.c_str(), "smash error: cd: too many arguments\n");
}

TEST(Smash, jobs) {
    const std::vector<std::string> in{"sleep 3 &", "sleep 3 &", "sleep 3 &", "jobs"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_TRUE(std::regex_match(captured_out,
                                 std::regex{"\\[1\\] sleep 3 & : \\d+ \\d+ secs\n"
                                            "\\[2\\] sleep 3 & : \\d+ \\d+ secs\n"
                                            "\\[3\\] sleep 3 & : \\d+ \\d+ secs\n"}));
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;
}

TEST(Smash, quit_sanity) {
    const std::vector<std::string> in{"quit"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_TRUE(captured_out.empty()) << "out.str() = " << captured_out;
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;
}

TEST(Smash, quit_kill) {
    const std::vector<std::string> in{"cat - &", " sort -&", " uniq -&", "quit kill"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_TRUE(std::regex_match(captured_out,
                                 std::regex{"smash: sending SIGKILL signal to 3 jobs:\n"
                                            "\\d+: cat - &\n"
                                            "\\d+:  sort -&\n"
                                            "\\d+:  uniq -&\n"}))
        << "out.str() = \"" << captured_out << '"';
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;
}

TEST(Smash, kill) {
    const std::vector<std::string> in{
        "sleep 100 &", "sleep 200 &", " kill -9 1", " jobs", " kill -19 2", " jobs", " kill -9 2", " jobs"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_TRUE(std::regex_match(captured_out,
                                 std::regex{"signal number 9 was sent to pid \\d+\n"
                                            "\\[2\\] sleep 200 & : \\d+ \\d+ secs\n"
                                            "signal number 19 was sent to pid \\d+\n"
                                            "\\[2\\] sleep 200 & : \\d+ \\d+ secs \\(stopped\\)\n"
                                            "signal number 9 was sent to pid \\d+\n"}))

        << "out.str() = \"" << captured_out << '"';
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;
}

TEST(Smash, kill_err) {
    const std::vector<std::string> in{"kill -1 1", "sleep 2 &", "kill -65 1", "kill 1 1", "kill", "kill -1 1 1\n"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_TRUE(captured_out.empty()) << "out.str() = " << captured_out;
    EXPECT_STREQ(captured_err.c_str(),
                 "smash error: kill: job-id 1 does not exist\n"
                 "smash error: kill failed: Invalid argument\n"
                 "smash error: kill: invalid arguments\n"
                 "smash error: kill: invalid arguments\n"
                 "smash error: kill: invalid arguments\n");
}

TEST(Smash, bg) {
    const std::vector<std::string> in{
        "sleep 5 &", "sleep 6 &", "kill -19 1", "kill -19 2", "bg", "bg", "kill -19 1", "kill -19 2", "bg 1", "bg 2"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_TRUE(std::regex_match(captured_out,
                                 std::regex{"signal number 19 was sent to pid \\d+\n"
                                            "signal number 19 was sent to pid \\d+\n"
                                            "sleep 6 & : \\d+\n"
                                            "sleep 5 & : \\d+\n"
                                            "signal number 19 was sent to pid \\d+\n"
                                            "signal number 19 was sent to pid \\d+\n"
                                            "sleep 5 & : \\d+\n"
                                            "sleep 6 & : \\d+\n"}))
        << "out.str() = \"" << captured_out << '"';
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;

    smash.quit(true);
}

TEST(Smash, bg_errors) {
    const std::vector<std::string> in{"bg 1", " bg", " bg 1 1", "sleep 1 &", "bg 1"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_TRUE(captured_out.empty()) << "out.str() = " << captured_out;
    EXPECT_STREQ(captured_err.c_str(),
                 "smash error: bg: job-id 1 does not exist\n"
                 "smash error: bg: there is no stopped jobs to resume\n"
                 "smash error: bg: invalid arguments\n"
                 "smash error: bg: job-id 1 is already running in the background\n");
}

TEST(Smash, fg) {
    const std::vector<std::string> in{
        "sleep 3 &", "sleep 5 &", "sleep 4 &", " fg 1", " fg", "sleep 3 &", " fg 2", " fg", " jobs"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_TRUE(std::regex_match(captured_out,
                                 std::regex{"sleep 3 & : \\d+\n"
                                            "sleep 4 & : \\d+\n"
                                            "sleep 5 & : \\d+\n"
                                            "sleep 3 & : \\d+\n"}))
        << "out.str() = \"" << captured_out << '"';
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;
}

TEST(Smash, fg_errors) {
    const std::vector<std::string> in{"fg 1", " fg", " fg 1 1"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_TRUE(captured_out.empty()) << "out.str() = " << captured_out;
    EXPECT_STREQ(captured_err.c_str(),
                 "smash error: fg: job-id 1 does not exist\n"
                 "smash error: fg: jobs list is empty\n"
                 "smash error: fg: invalid arguments\n");
}

TEST(Smash, getfiletype) {
    const std::vector<std::string> in{"cd ..", " getfiletype README.txt", " getfiletype test"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_STREQ(captured_out.c_str(),
                 "README.txt's type is \"regular file\" and takes up 3096 bytes\n"
                 "test's type is \"directory\" and takes up 4096 bytes\n");
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;
}

TEST(Smash, getfiletype_errors) {
    const std::vector<std::string> in{"getfiletype", " getfiletype a b"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_TRUE(captured_out.empty()) << "out.str() = " << captured_out;
    EXPECT_STREQ(captured_err.c_str(),
                 "smash error: getfiletype: invalid arguments\n"
                 "smash error: getfiletype: invalid arguments\n");
}

TEST(Smash, getfiletype_symlink) {
    const std::vector<std::string> in{
        "touch test", "ln -s test symlink_test", "getfiletype symlink_test", "rm symlink_test", "rm test"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_TRUE(std::regex_match(captured_out,
                                 std::regex{"symlink_test's type is \"symbolic link\" and takes up \\d+ bytes\n"}))
        << "captured_out = \"" << captured_out << '"';
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;
}

TEST(Smash, chmod) {
    const std::vector<std::string> in{"touch tmp_chmod_test_file\n",
                                      "chmod 777 tmp_chmod_test_file\n",
                                      "stat -c %A tmp_chmod_test_file\n",
                                      "chmod 2666 tmp_chmod_test_file\n",
                                      "stat -c %A tmp_chmod_test_file\n",
                                      "rm tmp_chmod_test_file\n"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};

    EXPECT_STREQ(captured_out.c_str(), "-rwxrwxrwx\n-rw-rwSrw-\n");
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;
}

TEST(Smash, chmod_errors) {
    const std::vector<std::string> in{"chmod\n", "chmod 0 a a\n", "chmod -53 a\n"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_TRUE(captured_out.empty()) << "out.str() = " << captured_out;
    EXPECT_STREQ(captured_err.c_str(),
                 "smash error: chmod: invalid arguments\n"
                 "smash error: chmod: invalid arguments\n"
                 "smash error: chmod: invalid arguments\n");
}

TEST(Smash, pipe_external_to_external) {
    const std::vector<std::string> in{"echo a | tr a b"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_STREQ(captured_out.c_str(), "b\n");
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;
}

TEST(Smash, pipe_builtin_to_external) {
    const std::vector<std::string> in{"showpid | tr -d 0-9"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_STREQ(captured_out.c_str(), "smash pid is \n");
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;
}

TEST(Helper, strip_word) {
    EXPECT_EQ("a", strip_word("    a   "));
    EXPECT_EQ("a", strip_word("a   "));
    EXPECT_EQ("a", strip_word("    a"));
    EXPECT_EQ("a", strip_word("    a   "));
    EXPECT_EQ("a", strip_word("a"));

    EXPECT_EQ("the;long>long<#$", strip_word("    the;long>long<#$   "));

    EXPECT_EQ("space_and_tab", strip_word(" space_and_tab   "));

    EXPECT_EQ("special_chars_><!@#$%^&*()_+-=_in_between",
              strip_word("  special_chars_><!@#$%^&*()_+-=_in_between    "));
    EXPECT_EQ("special_chars_><!@#$%^&*()_+-=_in_between", strip_word("special_chars_><!@#$%^&*()_+-=_in_between"));

#ifndef NDEBUG
    EXPECT_DEATH(strip_word("  too many words  "), ".*\\!\\(strip >> word\\).*");
#else
    EXPECT_STREQ(strip_word("  too many words  ").c_str(), "too");
#endif

    EXPECT_EQ("a", strip_word(" a\n   "));
    EXPECT_EQ("a", strip_word(" \na   "));
    EXPECT_EQ("a", strip_word("\na\n"));

    EXPECT_EQ("&", strip_word(" &   "));
    EXPECT_EQ("a&", strip_word(" a&   "));


    EXPECT_EQ("$", strip_word("                    $   "));
    EXPECT_EQ("\\", strip_word("        \\         "));
    EXPECT_EQ("/", strip_word("        /        "));
}

TEST(Smash, redirection_external) {
    const std::vector<std::string> in{"echo a > redirection_test.txt",
                                      "cat redirection_test.txt",
                                      "echo b > redirection_test.txt",
                                      "cat redirection_test.txt",
                                      "echo c >> redirection_test.txt",
                                      "cat redirection_test.txt",
                                      "rm -f redirection_test.txt",
                                      "echo a >> redirection_test.txt",
                                      "cat redirection_test.txt",
                                      "rm -f redirection_test.txt"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_STREQ(captured_out.c_str(), "a\nb\nb\nc\na\n");
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;
}

TEST(Smash, redirection_builtin) {
    const std::vector<std::string> in{"sleep 10 &",
                                      "jobs > redirection_test.txt",
                                      "cat redirection_test.txt",
                                      "sleep 20 &",
                                      "jobs > redirection_test.txt",
                                      "cat redirection_test.txt",
                                      "sleep 30 &",
                                      "jobs >> redirection_test.txt",
                                      "cat redirection_test.txt",
                                      "rm -f redirection_test.txt",
                                      "sleep 40 &",
                                      "jobs >> redirection_test.txt",
                                      "cat redirection_test.txt",
                                      "rm -f redirection_test.txt"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_TRUE(std::regex_match(captured_out,
                                 std::regex{"\\[1\\] sleep 10 & : \\d+ \\d+ secs\n"
                                            "\\[1\\] sleep 10 & : \\d+ \\d+ secs\n"
                                            "\\[2\\] sleep 20 & : \\d+ \\d+ secs\n"
                                            "\\[1\\] sleep 10 & : \\d+ \\d+ secs\n"
                                            "\\[2\\] sleep 20 & : \\d+ \\d+ secs\n"
                                            "\\[1\\] sleep 10 & : \\d+ \\d+ secs\n"
                                            "\\[2\\] sleep 20 & : \\d+ \\d+ secs\n"
                                            "\\[3\\] sleep 30 & : \\d+ \\d+ secs\n"
                                            "\\[1\\] sleep 10 & : \\d+ \\d+ secs\n"
                                            "\\[2\\] sleep 20 & : \\d+ \\d+ secs\n"
                                            "\\[3\\] sleep 30 & : \\d+ \\d+ secs\n"
                                            "\\[4\\] sleep 40 & : \\d+ \\d+ secs\n"}));
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;

    smash.quit(true);
}

TEST(Smash, not_real_command) {
    const std::vector<std::string> in{"bla bla", "bla bla"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_STREQ(captured_out.c_str(), "");
    EXPECT_STREQ(captured_err.c_str(),
                 "smash error: execvp failed: No such file or directory\n"
                 "smash error: execvp failed: No such file or directory\n");
    smash.quit(true);
}

TEST(Smash, redirection_nothing_from_error) {
    const std::vector<std::string> in{"bla bla > redirection_test.txt",
                                      "cat redirection_test.txt",
                                      "bla bla >> redirection_test.txt",
                                      "cat redirection_test.txt",
                                      "bg > redirection_test.txt",
                                      "cat redirection_test.txt",
                                      "bg >> redirection_test.txt",
                                      "cat redirection_test.txt",
                                      "rm -f redirection_test.txt"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_STREQ(captured_out.c_str(), "");
    EXPECT_STREQ(captured_err.c_str(),
                 "smash error: execvp failed: No such file or directory\n"
                 "smash error: execvp failed: No such file or directory\n"
                 "smash error: bg: there is no stopped jobs to resume\n"
                 "smash error: bg: there is no stopped jobs to resume\n");
    smash.quit(true);
}

TEST(Smash, SIGCONT_equals_to_bg) {
    const std::vector<std::string> in{"sleep 100&",
                                      "sleep 200&",
                                      "kill -19 1",
                                      "kill -19 2",
                                      "jobs",
                                      "kill -" + std::to_string(SIGCONT) + " 1",
                                      "jobs",
                                      "bg",
                                      "jobs"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_TRUE(std::regex_match(captured_out,
                                 std::regex{"signal number 19 was sent to pid \\d+\n"
                                            "signal number 19 was sent to pid \\d+\n"
                                            "\\[1\\] sleep 100& : \\d+ \\d+ secs \\(stopped\\)\n"
                                            "\\[2\\] sleep 200& : \\d+ \\d+ secs \\(stopped\\)\n"
                                            "signal number 18 was sent to pid \\d+\n"
                                            "\\[1\\] sleep 100& : \\d+ \\d+ secs\n"
                                            "\\[2\\] sleep 200& : \\d+ \\d+ secs \\(stopped\\)\n"
                                            "sleep 200& : \\d+\n"
                                            "\\[1\\] sleep 100& : \\d+ \\d+ secs\n"
                                            "\\[2\\] sleep 200& : \\d+ \\d+ secs\n"}))
        << "out.str() = " << captured_out;
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;

    smash.quit(true);
}

TEST(Smash, kill_signal_1) {
    const std::vector<std::string> in{"sleep 100&", "kill -1 1", "sleep 1", "jobs"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};
    EXPECT_TRUE(std::regex_match(captured_out, std::regex{"signal number 1 was sent to pid \\d+\n"}))
        << "out.str() = CAPTURE-BEGINS-NEXT-LINE\n"
        << captured_out << "END-OF-STDOUT-CAPTURE";
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;

    smash.quit(true);
}
