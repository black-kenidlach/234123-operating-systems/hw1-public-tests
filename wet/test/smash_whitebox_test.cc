#define private public
#include "Commands.h"
#undef private

#include "signals.h"

#include <gtest/gtest.h>
#include <sys/sysinfo.h>

TEST(Smash, setcore_to_0) {
    cpu_set_t cpu_set;

    const std::vector<std::string> in{"../infinite_loop &", "setcore 1 0"};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};

    EXPECT_TRUE(captured_out.empty()) << "out.str() = " << captured_out;
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;

    ASSERT_EQ(0, sched_getaffinity(smash.job_id_to_pid(1), sizeof(decltype(cpu_set)), &cpu_set));
    EXPECT_NE(0, CPU_ISSET(0, &cpu_set));

    smash.quit(true);
}

TEST(Smash, setcore_to_highest) {
    const int highest_core_num{get_nprocs() - 1};
    EXPECT_TRUE(0 <= highest_core_num);

    cpu_set_t cpu_set;

    const std::vector<std::string> in{"cd ..", "./infinite_loop &", "setcore 1 " + std::to_string(highest_core_num)};
    SmallShell smash{};
    ::testing::internal::CaptureStdout();
    ::testing::internal::CaptureStderr();
    for (const std::string &command : in) {
        smash.execute_pipeline(command);
    }
    const std::string captured_out{::testing::internal::GetCapturedStdout()};
    const std::string captured_err{::testing::internal::GetCapturedStderr()};

    EXPECT_TRUE(captured_out.empty()) << "out.str() = " << captured_out;
    EXPECT_TRUE(captured_err.empty()) << "error.str() = " << captured_err;

    ASSERT_EQ(0, sched_getaffinity(smash.job_id_to_pid(1), sizeof(decltype(cpu_set)), &cpu_set));
    EXPECT_NE(0, CPU_ISSET(static_cast<unsigned int>(highest_core_num), &cpu_set));

    smash.quit(true);
}
