#!/usr/bin/env sh

set -Eeuxo pipefail

cmake -S . -B build "$@"
cmake --build build
oldpwd=$(pwd)
cd build
    ctest --output-on-failure
    identifier=$(echo "$*" | sed -e 's/-D\(ENABLE_\)\?//g' | tr ' ' '_')
    log_path="./memcheck_output_${identifier}.txt"
    ninja test_memcheck | tee "${log_path}"
    last_line=$(tail -n 1 "${log_path}")

    [ "${last_line}" = "Memory checking results:" ]
cd "${oldpwd}"
